CERTS_DIR="./certs"
mkdir -p "${CERTS_DIR}"
rm -rf ${CERTS_DIR}/*

suffix="tcp-2"

# Private CA key
openssl genrsa -out "${CERTS_DIR}/ca.key.pem" 4096

# CA public cert
openssl req -key "${CERTS_DIR}/ca.key.pem" -new -x509 -days 7300 -sha256 -out "${CERTS_DIR}/ca.cert.pem" -extensions v3_ca -subj "/C=US/ST=Texas/L=Austin/O=OpenStack/OU=Magnum/CN=cci-example-CA-backend-$suffix"

# Private server-server key
openssl genrsa -out "${CERTS_DIR}/server.key.pem" 4096

# Request for server-server cert
openssl req -key "${CERTS_DIR}/server.key.pem" -new -sha256 -out "${CERTS_DIR}/server.csr.pem" -subj "/C=US/ST=Texas/L=Austin/O=OpenStack/OU=Magnum/CN=server-backend-$suffix"

# Sign server-server cert
openssl x509 -req -CA "${CERTS_DIR}/ca.cert.pem" -CAkey "${CERTS_DIR}/ca.key.pem" -CAcreateserial -in "${CERTS_DIR}/server.csr.pem" -out "${CERTS_DIR}/server.cert.pem" -days 365

kubectl delete secret $suffix
kubectl create secret generic $suffix --from-file=certs/server.key.pem --from-file=certs/server.cert.pem
