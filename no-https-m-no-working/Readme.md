Exposing the same TCP port is not possible as expected.

The horrible part is that there is not warning about it.
i.e. for:

tcp:
  13000: "default/tcp-1:13000"
  13000: "default/tcp-2:14000"

only 13000: "default/tcp-2:14000" is taken into account in
nginx's config.
