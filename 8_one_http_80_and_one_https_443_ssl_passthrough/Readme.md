Example of two services running openresty.

svc1.cern.ch is configured with ssl-passthrough and a self-signed certificate listening on port 443.

svc2.cern.ch listens on port 80 with plain http.

Both services are served by the same ingress controller.
