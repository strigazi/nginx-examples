CERTS_DIR="./certs"
mkdir -p "${CERTS_DIR}"
rm -rf ${CERTS_DIR}/*

# svc1 will be over ssl
suffix="svc1"

### One CA which will sign the certificate for the ssl-passthrough service

# Private CA key
openssl genrsa -out "${CERTS_DIR}/ca.key.pem" 4096

# CA public cert
openssl req -key "${CERTS_DIR}/ca.key.pem" -new -x509 -days 7300 -sha256 -out "${CERTS_DIR}/ca.cert.pem" -extensions v3_ca -subj "/C=US/ST=Texas/L=Austin/O=OpenStack/OU=Magnum/CN=cci-example-CA-backend-$suffix"

### Create the certificate for the service
# Private server-server key
openssl genrsa -out "${CERTS_DIR}/server.key.pem" 4096

# Request for server-server cert
openssl req -key "${CERTS_DIR}/server.key.pem" -new -sha256 -out "${CERTS_DIR}/server.csr.pem" -subj "/C=US/ST=Texas/L=Austin/O=OpenStack/OU=Magnum/CN=$suffix"

# Sign server-server cert
openssl x509 -req -CA "${CERTS_DIR}/ca.cert.pem" -CAkey "${CERTS_DIR}/ca.key.pem" -CAcreateserial -in "${CERTS_DIR}/server.csr.pem" -out "${CERTS_DIR}/server.cert.pem" -days 365

# Create the secret in kubernetes
kubectl delete secret server-backend-$suffix
kubectl create secret generic server-backend-$suffix --from-file=certs/server.key.pem --from-file=certs/server.cert.pem
