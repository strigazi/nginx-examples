#
#   svc1
#
$ curl -s -v -k https://svc1.cern.ch
* About to connect() to svc1.cern.ch port 443 (#0)
*   Trying 188.184.82.107...
* Connected to svc1.cern.ch (188.184.82.107) port 443 (#0)
* Initializing NSS with certpath: sql:/etc/pki/nssdb
* skipping SSL peer certificate verification
* SSL connection using TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
* Server certificate:
* 	subject: CN=svc1,OU=Magnum,O=OpenStack,L=Austin,ST=Texas,C=US
* 	start date: Jun 19 14:20:18 2020 GMT
* 	expire date: Jun 19 14:20:18 2021 GMT
* 	common name: svc1
* 	issuer: CN=cci-example-CA-backend-svc1,OU=Magnum,O=OpenStack,L=Austin,ST=Texas,C=US
> GET / HTTP/1.1
> User-Agent: curl/7.29.0
> Host: svc1.cern.ch
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: openresty/1.15.8.3
< Date: Fri, 19 Jun 2020 14:28:35 GMT
< Content-Type: text/html
< Content-Length: 5
< Last-Modified: Fri, 19 Jun 2020 14:20:43 GMT
< Connection: keep-alive
< ETag: "5eecc9bb-5"
< Accept-Ranges: bytes
< 
{ [data not shown]
* Connection #0 to host svc1.cern.ch left intact
svc1



#
#   svc2
#
$ curl -s -v http://svc2.cern.ch 
* About to connect() to svc2.cern.ch port 80 (#0)
*   Trying 188.184.82.107...
* Connected to svc2.cern.ch (188.184.82.107) port 80 (#0)
> GET / HTTP/1.1
> User-Agent: curl/7.29.0
> Host: svc2.cern.ch
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: nginx/1.17.10
< Date: Fri, 19 Jun 2020 14:29:40 GMT
< Content-Type: text/html
< Content-Length: 5
< Connection: keep-alive
< Last-Modified: Fri, 19 Jun 2020 14:25:33 GMT
< ETag: "5eeccadd-5"
< Accept-Ranges: bytes
< 
{ [data not shown]
* Connection #0 to host svc2.cern.ch left intact
svc2
