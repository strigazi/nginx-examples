Different scenarios exposing http or TCP services with nginx-ingress.

| #  | Scenario                                                                                                   |
| -- | ---------------------------------------------------------------------------------------------------------- |
| 1. | [443 ssl-passthrough](./https-p-sp)                                                                        |
| 2. | [443 tls termination](./https-p-t)                                                                         |
| 3. | [two svcs on 443 with tls termination](./https-p-t-m)                                                      |
| 4. | [two svcs on 13000 and 14000 with ssl-passthourgh](./no-https-m)                                           |
| 5. | [two svcs on 13000 with ssl-passthourh, DOES NOT work](./no-https-m-no-working)                            |
| 6. | [port 13000 ssl-passthrough](./no-https-p-sp)                                                              |
| 7. | [can not do tls termination with non http port](./no-https-p-t)                                            |
| 8. | [one http (80) and one https (443) w/ ssl-passthrough](./8_one_http_80_and_one_https_443_ssl_passthrough) |

